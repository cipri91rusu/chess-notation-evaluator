﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ChessMoves
{
    public class FileInput
    {
        public string GetFileContent() => 
            new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "Moves.txt")).ReadToEnd().Replace("\r\n", "");
    }
}