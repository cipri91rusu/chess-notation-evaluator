﻿using ChessGame.Interfaces;
using System;
using System.Collections.Generic;

namespace ChessMoves
{
    public interface IUserMove : ILocation, IPlayer, INotation, IPieceType
    {
        void UpdatePieceCallback(Action<(int, int), Player> updateAction);
        void RemovePieceCallback(Action<(int, int)> removeAction);
        void MovePieceCallback(Action<IPiece, (int, int)> action);
        bool CanHandle(IPieceState pieceStats, IMoveCheck boardCheck, IPiece[,] board);
        void Perform(IMovePerform boardMove, IPiece[,] board);
    }
}
