﻿using ChessGame.Interfaces;
using ChessMoves.Paths;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChessMoves
{
    public abstract class Piece : IPiece, IPieceState
    {
        private readonly Index Convertor = new Index();

        public Piece(string index, Player player)
        {
            Index = Convertor.GetIndex(index);
            Player = player;
            File = index.First();
            Rank = index.Last();
            WasMoved = false;
            IsPassantCapturable = false;
        }
        public (int, int) Index { get; internal set; }
        public Player Player { get; internal set; }
        public char File { get; private set; }
        public char Rank { get; private set; }
        public bool WasMoved { get; private set; }
        public bool IsPassantCapturable { get; private set; }
        public Type PieceType { get; internal set; }
        public virtual IEnumerable<IPath> Moves { get; private set; }
        public virtual IEnumerable<IPath> Captures { get; private set; }
        public virtual bool CanPerform(IUserMove move, IMoveCheck moveCheck, IPiece[,] board) => move.CanHandle(this, moveCheck, board);
        public virtual void Update(IUserMove move)
        {
            if(IsPassantCapturable == true) {
                IsPassantCapturable = false;
            }

            if(!WasMoved)
            {
                if(Player == Player.Black && Rank == '7' && this is Pawn)
                {
                    if(move.Index.Item1 - Index.Item1 == 2) {
                        IsPassantCapturable = true;
                    }
                }
                if(Player == Player.White && Rank == '2' && this is Pawn)
                {
                    if(move.Index.Item1 - Index.Item1 == -2) {
                        IsPassantCapturable = true;
                    }
                }
            }

            var rankAndFile = new RankAndFile(move.Index);
            Index = move.Index;
            File = rankAndFile.File;
            Rank = rankAndFile.Rank;
            WasMoved = true;
        }
    }
}