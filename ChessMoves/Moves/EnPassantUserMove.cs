﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ChessGame.Interfaces;

namespace ChessMoves.Moves
{
    public class EnPassantUserMove : UserMove, IUserMove
    {
        public EnPassantUserMove(string input, Player playerTurn) : base(input, playerTurn) { }

        public override bool CanHandle(IPieceState pieceState, IMoveCheck moveCheck, IPiece[,] board)
        {
            if (pieceState.PieceType == PieceType && pieceState.Player == Player)
            {
                var path = pieceState.Captures.Where(x => x.End == Index);

                if (Player == Player.White && board[Index.Item1 + 1, Index.Item2].IsPassantCapturable == true)
                {
                    return path.Any() && path.Any() && moveCheck.IsClear(path.Single());
                }

                if (Player == Player.Black && board[Index.Item1 - 1, Index.Item2].IsPassantCapturable == true)
                {
                    return path.Any() && path.Any() && moveCheck.IsClear(path.Single());
                }
            }

            return false;
        }

        public override void Perform(IMovePerform boardMove, IPiece[,] board)
        {
            var current = boardMove.Performers.Single();

            MovePiece(current, Index);
            
            current.Update(this);

            if (Player == Player.White)
            {
                RemovePiece((Index.Item1 + 1, Index.Item2));
            }

            if (Player == Player.Black)
            {
                RemovePiece((Index.Item1 - 1, Index.Item2));
            }
        }
    }
}
