﻿using ChessMoves.Moves;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ChessGame.Interfaces;

namespace ChessMoves
{
    public class PromotionUserMove : UserMove, IUserMove
    {
        public PromotionUserMove(string input, Player playerTurn) : base(input, playerTurn) { }
        public override bool CanHandle(IPieceState pieceState, IMoveCheck moveCheck, IPiece[,] board)
        {
            if (pieceState.PieceType == PieceType && pieceState.Player == Player)
            {
                var CapturePaths = pieceState.Captures.Where(x => x.End == this.Index);

                var MovePaths = pieceState.Moves.Where(x => x.End == this.Index);

                return CapturePaths.Any() && moveCheck.IsClear(CapturePaths.Single()) || MovePaths.Any() && moveCheck.IsClear(MovePaths.Single());
            }

            return false;
        }

        public override void Perform(IMovePerform boardMove, IPiece[,] board)
        {
            try
            {
                var current = boardMove.Performers.Single();

                UpdatePiece(Index, this.Player);
                
            }
            catch (Exception)
            {
                throw new PieceException("Invalid Piece");
            }
        }
    }
}
