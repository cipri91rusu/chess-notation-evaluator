﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ChessGame.Interfaces;

namespace ChessMoves.Moves
{
    public class KingCastlingUserMove : UserMove, IUserMove
    {
        public KingCastlingUserMove(string input, Player turnToMove) : base(input, turnToMove) { }

        public override bool CanHandle(IPieceState pieceState, IMoveCheck moveCheck, IPiece[,] board)
        {
            if (Player == Player.Black)
            {

                if (pieceState is Rock && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (0, 7))
                {
                    return true;
                }

                if (pieceState is King && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (0, 4))
                {
                    return true;
                }

            }

            if (Player == Player.White)
            {
                if (pieceState is Rock && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (7, 7))
                {
                    return true;
                }

                if (pieceState is King && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (7, 4))
                {
                    return true;
                }
            }

            return false;
        }

        public override void Perform(IMovePerform boardMove, IPiece[,] board)
        {
            var rock = boardMove.Performers.Where(x => x is Rock).Single();
            var king = boardMove.Performers.Where(x => x is King).Single();

            if (Player == Player.Black)
            {
                MovePiece(rock, (0, 5));
                MovePiece(king, (0, 6));

                rock.Update(new StandardUserMove("f8", Player.Black));
                king.Update(new StandardUserMove("g8", Player.Black));
            }

            if (Player == Player.White)
            {
                MovePiece(rock, (7, 5));
                MovePiece(king, (7, 6));

                rock.Update(new StandardUserMove("f1", Player.Black));
                king.Update(new StandardUserMove("g1", Player.Black));
            }
        }
    }
}
