﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChessGame.Interfaces;

namespace ChessMoves.Moves
{
    public class QueenCastlingUserMove : UserMove, IUserMove
    {
        public QueenCastlingUserMove(string input, Player playerTurn) : base(input, playerTurn) { }

        public override bool CanHandle(IPieceState pieceState, IMoveCheck moveCheck, IPiece[,] board)
        {
            if (Player == Player.Black)
            {
                if (pieceState is Rock && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (0, 0))
                {
                    return true;
                }

                if (pieceState is King && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (0, 4))
                {
                    return true;
                }
            }

            if (Player == Player.White)
            {

                if (pieceState is Rock && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (7, 0))
                {
                    return true;
                }

                if (pieceState is King && !pieceState.WasMoved && pieceState.Player == Player && pieceState.Index == (7, 4))
                {
                    return true;
                }

            }

            return false;
        }

        public override void Perform(IMovePerform boardMove, IPiece[,] board)
        {
            var rock = boardMove.Performers.Where(x => x is Rock).Single();
            var king = boardMove.Performers.Where(x => x is King).Single();

            if (Player == Player.Black)
            {
                MovePiece(rock, (0, 3));
                MovePiece(king, (0, 2));

                rock.Update(new StandardUserMove("c8", Player.Black));
                king.Update(new StandardUserMove("d8", Player.Black));
            }

            if (Player == Player.White)
            {
                MovePiece(rock, (7, 3));
                MovePiece(king, (7, 2));

                rock.Update(new StandardUserMove("c1", Player.Black));
                king.Update(new StandardUserMove("d1", Player.Black));
            }
        }
    }
}
