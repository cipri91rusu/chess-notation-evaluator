﻿using ChessGame.Interfaces;
using ChessMoves.Moves;
using ChessMoves.Paths;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChessMoves
{
    public class Board : IBoard, IMoveCheck, IMovePerform
    {
        private const int ChessboardSize = 8;
        private IPiece[,] board = new IPiece[ChessboardSize, ChessboardSize];
        public Board(IPiece[,] board) => this.board = board;
        public bool IsClear(IPath path) => path.Path.All(x => board[x.Item1, x.Item2] == null);
        public bool IsCheck { get; private set; }
        public bool IsCheckMate { get; private set; }
        public IEnumerable<IPiece> Performers { get; private set; }
        public void GetCurrent(IUserMove move)
        {
            try
            {
                Performers = AllPieces().Where(x =>
                x != null &&
                x.CanPerform(move, this, board) &&
                new MoveConstraintValidator(x, move).IsValid);

                move.MovePieceCallback(MoveTo);

                move.RemovePieceCallback(RemoveElement);

                move.UpdatePieceCallback(UpdateElement);

                if (move is QueenCastlingUserMove && move.Player == Player.Black)
                {
                    if (board[0, 1] != null || board[0, 2] != null || board[0, 3] != null)
                    {
                        throw new UserMoveException(move, "Invalid move, path is not clear");
                    }

                    var opponents = AllPieces().Where(x => x != null && x.Player == Player.White);

                    var res = opponents.SelectMany(x => x.Captures.Where(x => x.End == (0, 1) && IsClear(x) || x.End == (0, 2) && IsClear(x) || x.End == (0, 3) && IsClear(x))).Count();

                    if (res != 0)
                    {
                        throw new UserMoveException(move, "Invalid move, casting through check!");
                    }
                }

                if (move is QueenCastlingUserMove && move.Player == Player.White)
                {
                    if (board[7, 1] != null || board[7, 2] != null || board[7, 3] != null)
                    {
                        throw new UserMoveException(move, "Invalid move, path is not clear");
                    }

                    var opponents = AllPieces().Where(x => x != null && x.Player == Player.Black);

                    var res = opponents.SelectMany(x => x.Captures.Where(x => x.End == (7, 1) && IsClear(x) || x.End == (7, 2) && IsClear(x) || x.End == (7, 3) && IsClear(x))).Count();

                    if (res != 0)
                    {
                        throw new UserMoveException(move, "Invalid move, casting through check!");
                    }
                }

                if (move is KingCastlingUserMove && move.Player == Player.Black)
                {
                    if (board[0, 5] != null || board[0, 6] != null)
                    {
                        throw new UserMoveException(move, "Invalid move, path is not clear");
                    }

                    var opponents = AllPieces().Where(x => x != null && x.Player == Player.White);

                    var res = opponents.SelectMany(x => x.Captures.Where(x => x.End == (0, 5) && IsClear(x) || x.End == (0, 6) && IsClear(x))).Count();

                    if (res != 0)
                    {
                        throw new UserMoveException(move, "Invalid move, casting through check!");
                    }
                }

                if (move is KingCastlingUserMove && move.Player == Player.White)
                {
                    if (board[7, 5] != null || board[7, 6] != null)
                    {
                        throw new UserMoveException(move, "Invalid move, path is not clear");
                    }

                    var opponents = AllPieces().Where(x => x != null && x.Player == Player.Black);

                    var res = opponents.SelectMany(x => x.Captures.Where(x => x.End == (7, 5) && IsClear(x) || x.End == (7, 6) && IsClear(x))).Count();

                    if (res != 0)
                    {
                        throw new UserMoveException(move, "Invalid move, casting through check!");
                    }
                }

                move.Perform(this, board);
            }
            catch (Exception)
            {
                throw new UserMoveException(move, "Invalid move");
            }

            if (IsCheck == false && move is KingCheckUserMove)
            {
                var performer = board[move.Index.Item1, move.Index.Item2];

                var pathToKing = performer.Captures.Where(x =>
                x != null &&
                board[x.End.Item1, x.End.Item2] is King &&
                board[x.End.Item1, x.End.Item2].Player != move.Player);
                try
                {
                    var isClear = IsClear(pathToKing.Single());
                    IsCheck = pathToKing != null && isClear;
                }
                catch (Exception)
                {
                    throw new UserMoveException(move, "Invalid check move");
                }
            }

            if (IsCheckMate == true && !(move is KingCheckMateUserMove))
            {
                throw new UserMoveException(move, "Invalid move, King is in CheckMate");
            }

            if (IsCheckMate == false && move is KingCheckMateUserMove)
            {
                var performer = board[move.Index.Item1, move.Index.Item2];

                var pathToKing = performer.Captures.Where(x =>
                x != null &&
                board[x.End.Item1, x.End.Item2] is King &&
                board[x.End.Item1, x.End.Item2].Player != move.Player);

                IsCheckMate = true;
            }

            if (IsCheck == true && !(move is KingCheckUserMove))
            {
                var currentKing = AllPieces().Where(x =>
                x != null &&
                x is King &&
                x.Player == move.Player).Single();

                var opponents = AllPieces().Where(x =>
                x != null &&
                x.Player != move.Player);

                var res = opponents.SelectMany(x => x.Captures.Where(x => x.End == currentKing.Index && IsClear(x))).Count();

                if (res == 0)
                {
                    IsCheck = false;
                }
                else
                {
                    throw new UserMoveException(move, "Invalid move, King is still in check");
                }
            }
        }

        private void MoveTo(IPiece piece, (int, int) Target)
        {
            board[Target.Item1, Target.Item2] = piece;
            board[piece.Index.Item1, piece.Index.Item2] = null;
        }

        private void RemoveElement((int index1, int index2) valueIndex) => 
        board[valueIndex.index1, valueIndex.index2] = null;

        private void UpdateElement((int index1, int index2) valueIndex, Player player) => 
        board[valueIndex.index1, valueIndex.index2] = new Queen(new RankAndFile(valueIndex).GetRankAndFile, player);

        private IEnumerable<IPiece> AllPieces()
        {
            var allIndices = Enumerable.Range(0, ChessboardSize);
            return allIndices.SelectMany(x => allIndices.Select(y => board[x, y]));
        }
    }
}
