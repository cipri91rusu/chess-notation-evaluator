# Algebraic Chess Notation Evaluator

### Demo project, used to evaluate Algebraic Chess Notation in a (primitive) graphical UI, while providing several details regarding the state of the game.

***

**Algebraic Chess Notation** is a way to adnotate every step of a chess game, providing as many details so that the game can be afterwards recreated. Mainly, every record contains the piece (or pieces) being moved, origin & destination, and type (simple move, capture, special moves), currently being recognized as the standard way of recording professional chess games by the International Chess Federation (FIDE). As such, many famous games can be recreated using this type of notation.

More details can be found at https://en.wikipedia.org/wiki/Algebraic_notation_(chess)

In it's current shape, the project is divided in two subprojects: **ChessMoves** (which contains all the logic pertaining the game, pieces, legal moves, etc) and **DisplayChessBoard**, which receives the final shape of the game and renders the board in it's final state (after all moves had been applied to the board).

>As the focus was on the Parsing/Notation aspect, the UI still supports much improvement, at the moment being primitively rendered inside the console, ASCII
>like, rendering any messages below (the state of the board and any special states such as Check or Checkmate)
